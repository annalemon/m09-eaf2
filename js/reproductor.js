var so = new Audio("media/tipus_imatge.mp3");

function reproduir(){
	if (so.paused){
		so.play();
	} else {
		so.pause();
	}
}

function pujarVolum(){
	if(so.volume != 1){
		so.volume += 0.1;
	}
}

function baixarVolum(){
	if(so.volume != 0){
		so.volume -= 0.1;
	}
}

function silenciar(){
	if (so.muted == false){
		so.muted = true;
	} else if (so.muted == true){
		so.muted = false;
	}
}